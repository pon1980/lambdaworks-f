namespace LambdaWorks.Shims.Ocaml

open System
open System.IO
open System.Text

module Buffer =
  exception Invalid_arguments
  exception Not_found
  exception End_of_file
  exception Not_implemented
  type t = StringBuilder
  let create (n : int) = StringBuilder n
  let contents (b : t) = b.ToString ()
  let to_bytes (b : t) = (b.ToString ()).ToCharArray ()
  let sub (b : t) i0 i1 = 
    try 
      b.ToString (i0, i1) 
    with 
    | :?  ArgumentOutOfRangeException -> raise Invalid_arguments
  let blit (b : t) s0 t t0 t1 = 
    try
      b.CopyTo (s0, t, t0, t1)
    with
    | :? ArgumentNullException -> raise Invalid_arguments
    | :? ArgumentOutOfRangeException -> raise Invalid_arguments
    | :? ArgumentException -> raise Invalid_arguments
  let nth (b : t) n = 
    try
      b.Chars(n)
    with
    | :? IndexOutOfRangeException -> raise Invalid_arguments
  let length (b : t) = b.Length
  let clear (b : t) = b.Clear () |> ignore
  let reset (b : t) = raise Not_implemented
  let add_char (b : t) = (b.Append : char -> StringBuilder) >> ignore
  let add_string (b : t) = (b.Append : string -> StringBuilder) >> ignore
  let add_bytes (b : t) = (b.Append : char[] -> StringBuilder) >> ignore
  let add_substring (b : t) s s0 s1 = (b.Append : string * int32 * int32 -> StringBuilder) (s, s0, s1) |> ignore
  let add_subbytes (b : t) c c0 c1 = (b.Append : char[] * int32 * int32 -> StringBuilder) (c, c0, c1) |> ignore
  let add_substitute (b : t) f s = raise Not_implemented
  let add_buffer (b : t) = (b.Append : StringBuilder -> StringBuilder) >> ignore
  let add_channel (b : t) (t : TextReader) n =
    let a = Array.create 1024 '\000' in
    let rec loop progress =
      if progress = n then ()
      else 
        let r = t.Read(a, 0, min 1024 (n - progress)) in
        let () = add_subbytes b a 0 r in
        if r = 0 then raise End_of_file
        else loop (progress + r)
    in
    loop 0
  let output_buffer (t : TextWriter) = contents >> (t.Write : string -> unit)


