namespace LambdaWorks.Parallel

module Agent =
  module Channel =
    type 'a t = AsyncReplyChannel<'a>
    let reply (ch : 'a t) msg = ch.Reply msg

  type 'a t = MailboxProcessor<'a>
  
  let create fn = new t<_>(fn)
  let createStarted fn = t.Start fn
  let start (agent : 'a t) = agent.Start ()
  let post (agent : 'a t) m = agent.Post m
  let receive (agent : 'a t) = agent.Receive ()
  let receiveTimeout timeout (agent : 'a t) = agent.Receive timeout
  let postAndReply (agent : 'a t) chFun = agent.PostAndReply chFun
  let postAndReplyTimeout timeout (agent : 'a t) chFun = agent.PostAndReply (chFun, timeout)
  let postAndAsyncReply (agent : 'a t) chFun = agent.PostAndReply chFun
  let postAndAsyncReplyTimeout timeout (agent : 'a t) chFun = agent.PostAndReply (chFun, timeout)
  let scan (agent : 'a t) scanner = agent.Scan scanner
  let scanTimeout timeout (agent : 'a t) scanner = agent.Scan (scanner, timeout)
  let tryScan (agent : 'a t) scanner = agent.TryScan scanner
  let tryScanTimeout timeout (agent : 'a t) scanner = agent.TryScan (scanner, timeout)
  let tryReceive (agent : 'a t) = agent.TryReceive ()
  let tryReceiveTimeout timeout (agent : 'a t) = agent.TryReceive timeout


