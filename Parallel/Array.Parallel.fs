namespace LambdaWorks.Parallel

open System
open System.Threading

module Array =
  let chunkIndices length n =
    let rec loop curr acc =
      let inc = min n (length - curr)
      if curr >= length then Array.ofList (List.rev acc)
      else loop (curr + inc) ((curr, curr + inc)::acc)
    loop 0 []

  module Parallel =
    module Interlocked =
      let iteri f a =
        let arrayLength = Array.length a
        let arrayIndex  = ref -1
        let rec loop () =
          let i = Interlocked.Increment arrayIndex
          if i >= arrayLength then ()
          else (f i a.[i];  loop ())
        [for i in 1..Environment.ProcessorCount -> i]
        |> List.map (fun _ -> async {do loop ()})
        |> Async.Parallel
        |> Async.Ignore
        |> Async.RunSynchronously
       
      let iter f = iteri (fun _ a -> f a)

      let init n f =
        let arrayResult = Array.zeroCreate n
        let () = iteri (fun i _ -> arrayResult.[i] <- f i) arrayResult
        arrayResult

      let mapi f a =
        let arrayResult = a |> Array.length |> Array.zeroCreate
        let () = iteri (fun i x -> arrayResult.[i] <- f i x) a
        arrayResult
        
      let map f = mapi (fun _ x -> f x)

      let foldi f init join a =
        let arrayLength = Array.length a
        let arrayIndex  = ref -1
        let rec loop acc =
          let i = Interlocked.Increment arrayIndex
          if i >= arrayLength then acc
          else loop (f acc i a.[i])
        [for i in 1..Environment.ProcessorCount -> i]
        |> List.map (fun _ -> async {return loop init})
        |> Async.Parallel
        |> Async.RunSynchronously
        |> join
      
      let fold f = foldi (fun acc i x -> f acc x)

    let iteri f a = 
      let arrayLength    = Array.length a
      let arrayChunkSize = arrayLength / Environment.ProcessorCount
      let rec loop curr stop =
        if curr >= stop then ()
        else (f curr a.[curr]; loop (curr + 1) stop)
      (arrayLength, arrayChunkSize) 
      ||> chunkIndices
       |> Array.map (fun (start, stop) -> async {do loop start stop})
       |> Async.Parallel
       |> Async.Ignore
       |> Async.RunSynchronously

    let iter f = iteri (fun _ x -> f x)

    let init n f =
      let arrayResult = Array.zeroCreate n
      let () = iteri (fun i _ -> arrayResult.[i] <- f i) arrayResult
      arrayResult

    let mapi f a =
      let arrayResult = a |> Array.length |> Array.zeroCreate
      let () = iteri (fun i x -> arrayResult.[i] <- f i x) a
      arrayResult
      
    let map f = mapi (fun _ x -> f x)
    
    let foldi f init join a =
      let arrayLength    = Array.length a
      let arrayChunkSize = arrayLength / Environment.ProcessorCount
      let rec loop curr stop acc =
        if curr >= stop then acc
        else loop (curr + 1) stop (f acc curr a.[curr])
      (arrayLength, arrayChunkSize) 
      ||> chunkIndices
       |> Array.map (fun (start, stop) -> async {return loop start stop init})
       |> Async.Parallel
       |> Async.RunSynchronously
       |> join

    let fold f = foldi (fun acc _ x -> f acc x)

