OS?=$(shell uname)
PROCESSOR_ARCHITECTURE?=$(uname -p)
ARCH:=$(PROCESSOR_ARCHITECTURE)
ifeq ($(OS),Windows_NT)
	FSI=fsi
	FSC=fsc
else
	FSI=fsharpi
	FSC=fsharpc
endif
RM=rm -f


.PHONY: default
default: shims parallel
	@echo Building for $(OS)/$(ARCH)...
	@printf "Using tools:\n\tFSI: $(FSI)\n\tFSC: $(FSC)\n"

.PHONY: shims
shims: LambdaWorks.Shims.dll

.PHONY: parallel
parallel: LambdaWorks.Parallel.dll

.PHONY: clean
clean: 
	$(RM) LambdaWorks.Shims.dll
	$(RM) LambdaWorks.Parallel.dll

LambdaWorks.Shims.dll: Shims/Buffer.fs
	$(FSC) --target:library --out:$@ $^

LambdaWorks.Parallel.dll: Parallel/Array.Parallel.fs Parallel/Agent.fs
	$(FSC) --target:library --out:$@ $^
